<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'gda' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`G@1#GI_Km:LV#qfgEcBE!yvmCSCA:AD4O6w<(FESi-(w?:2qvYg(`v$bS/=h=:~' );
define( 'SECURE_AUTH_KEY',  'XXPX{}Y|8B?&X*GeB3=jh$/B|3{-7H*e.i<`BEjQ!J%M :+>,_-X$[F!SU8:=%L-' );
define( 'LOGGED_IN_KEY',    'OYmCxbIEJI9mi?VCU.9E6/3t??C8iv]83Yy+kq&69;nyF(m=nz<syE8xzs3+)(62' );
define( 'NONCE_KEY',        'T>p.#6pci@J*vEgaoMW{N`3*pON-qL8E~<01E/BLFIFe;9*0!-nWcb=+F2JMlQ),' );
define( 'AUTH_SALT',        'XBAVsjH3eC(`Ch,iKcf6ER<s;2O#&gn=*(%HF%$e@),Qq;$a,v=_i+!H@~&#]}aX' );
define( 'SECURE_AUTH_SALT', '_z9,N /B|V:dxt^By_,#JKYXVb$$nA&%?H*cD4^.)290ur*u-q+;{nb4!N):Bm)u' );
define( 'LOGGED_IN_SALT',   ';;@J:G=]2u^@b#cmBo$43*Z~{dz9,:-2]Gm#>7/V:,?emYo~W#EiRKUgtyFi?z{x' );
define( 'NONCE_SALT',       'M1$}z1npS|M1!sOGKWFgB*?,DTyX1VB{Ii_fE%qhEX;Cs06Qy0I+p@lM~rBJb.j&' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
